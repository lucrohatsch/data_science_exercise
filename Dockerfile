FROM node:20-alpine3.18 AS front-builder
WORKDIR /frontend
COPY ./frontend .
RUN npm install
RUN npm run build

FROM python:3.10-slim-bullseye
ENV PYTHONUNBUFFERED=1

WORKDIR /

COPY requirements.txt requirements.txt
RUN python -m pip install --upgrade pip
RUN pip install --upgrade pip wheel\
    && pip install -r requirements.txt
COPY ./api ./api
COPY --from=front-builder /frontend/build /frontend/build
CMD python -m uvicorn api.main:app --env-file .env --host 0.0.0.0 --port 5001 --log-level info
