# data_science_exercise
This repository contains exercises for the evaluation of data science candidates.

## Job description
We are looking to strengthen our team with a Data Engineer with experience in technologies such as python, pyspark, elasticsearch, SQL.
Our candidates should have a strong understanding of ETL best practices and experience with microservices.
Also eperience with rest APIs and docker are valoreted.

## Evaluation
The evaluations has 2 parts:
- ETL geopositional data using MapLibre
- Classify general news

For both we expect to be functional and ideally deployed. For this, you must place your code in "exercise.py" backend file in order to retrive the final result to the frontend.
You are free to change any thing you want in the backend and frontend code. Just make it functional and keep it simple.
The first thing we are gonna do is to upload a new file with data and check if we get a correct resolution.


## Getting started
Clone repository // or fork it
```bash
git clone https://gitlab.com/lucrohatsch/data_science_exercise.git
```

create dev environ in local
```bash
make environ
```

build frontend
```bash
make build
```

run dev environ
```bash
make dev
```

in a browser go to [localhost](http://127.0.0.1:5001)
Also the backend documentation is in [swagger](http://127.0.0.1:5001/docs)

## Project structure

This repository builds a fullstack projects.
The frontend is just to uplad csv data files and check the result in a graphical way.
The backend has 2 endpoints to upload a csv file. These endpoints read the file and execute the code that you will create within the Geoposition and Classification classes.
In the data folder you will find two sets of data that you can use to develop your code and test it.
Remember, you are free to change anything you want, but keep ti simple.

## Exercises

### Exercise 1: ETL geopositional

The `geo_data.csv` file contents sevelas registers, some of them has a geo_point.

Create a script inside "Geoposition" class to get all the geo_points and return it as needed to build a [map](https://maplibre.org/maplibre-gl-js/docs/) centered on the area of greatest relevance.
As final result we should be abel to load a csv form the frontend with data and get a map centered.



### Exercise 2: News classifier

Crate a model to to classify news from newspapers based on the title. Use the data in `news_training_data.csv` to train your model.

As final result we should be abel to load a csv form the frontend with data and get a table with the clasification.
