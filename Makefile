environ:
	python3 -m venv .venv
	. .venv/bin/activate && pip install -r requirements.txt

dev:
	. .venv/bin/activate && uvicorn api.main:api --reload --port 5001 --log-level debug

run:
	docker-compose up -d

build:
	cd frontend/ && npm install && npm run build && cd ..

local:
	docker-compose -f docker-compose-local.yml up
