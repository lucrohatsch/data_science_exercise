import typing
import logging
from fastapi import FastAPI, UploadFile, HTTPException
from fastapi.staticfiles import StaticFiles
from api.cases.exercise import Geoposition, Classify
from api.dtos import BaseResponse, GeopointResponse, News
from fastapi.middleware.cors import CORSMiddleware


api = FastAPI(version="0.1", title="Data Science Exercise")

api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@api.get("/version")
async def version():
    return {
        "version": api.version
    }

@api.post("/geolocation/upload")
async def upload_data(csv_file: UploadFile) -> BaseResponse[GeopointResponse, typing.Dict]:
    if not csv_file.content_type in ["text/csv", "application/vnd.ms-excel"]:
        raise HTTPException(status_code=415, detail="File must be csv")
    content_read = await csv_file.read()
    result = await Geoposition(data=content_read).execute()
    return BaseResponse(data=result, errors={})

@api.post('/classifier/upload')
async def uplad_news(csv_file: UploadFile) -> BaseResponse[typing.List[News], typing.Dict]:
    if not csv_file.content_type in ["text/csv", "application/vnd.ms-excel"]:
        raise HTTPException(status_code=415, detail="File must be csv")
    content_read = await csv_file.read()
    result = await Classify(data=content_read).execute()
    return BaseResponse(data=result, errors={})


try:
    api.mount("/", StaticFiles(directory="frontend/build", html = True), name="static")
except Exception as error:
    logging.error(f"Error while loading frontend: {error}")
    logging.warning("Make sure you built it")


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(api, host="127.0.0.1", port=3000)
