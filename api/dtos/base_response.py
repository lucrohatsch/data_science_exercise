from pydantic import BaseModel
import typing

Data = typing.TypeVar("Data")
Errors = typing.TypeVar("Errors")

class BaseResponse(BaseModel, typing.Generic[Data, Errors]):
    data: Data
    errors: Errors
