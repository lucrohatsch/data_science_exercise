from pydantic import BaseModel
import typing



class News(BaseModel):
    id: int
    source: str
    type: str
    content: str
