from pydantic import BaseModel
import typing


class Point(BaseModel):
    type: str = "Point"
    coordinates: typing.List[float]


class CenterPoint(BaseModel):
    xCoordinate: float
    yCoordinate: float
    zoom: int

class Feature(BaseModel):
    # class prop(BaseModel):
    #     intensity: int
    type:str = "Feature"
    geometry: Point
    properties: str


class GeopointResponse(BaseModel):
    type: str = "FeatureCollection"
    features: typing.List[Feature]
    centerMap: CenterPoint



