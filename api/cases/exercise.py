from api.dtos import GeopointResponse, News
class Geoposition:
    def __init__(self, data) -> None:
        self.data = data

    async def execute(self) -> GeopointResponse:
        return 


class Classify:
    def __init__(self, data) -> None:
        self.data = data

    async def execute(self) -> News:
        return 
