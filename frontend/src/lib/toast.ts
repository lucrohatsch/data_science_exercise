import { toasts }  from "svelte-toasts";

const showToast = (title: string, description: string, type: any) => {
    const toast = toasts.add({
        title: title,
        description: description,
        duration: 5000, // 0 or negative to avoid auto-remove
        showProgress: true,
        placement: 'bottom-right',
        type: type,
        theme: 'dark',
        onClick: () => {},
        onRemove: () => {},
    });
};

export default showToast;