import axios from "axios";

let getHeaders = () => {
    const headers = {
        accept: 'application/json',
        'Content-Type': 'multipart/form-data'
    };
    return {headers: headers};
}

let fileToFormData = (file:File) => {
    let formData = new FormData();
    formData.append('csv_file', file)
    return formData;
}

export let postGeolocationUpload = async (fileCsv: File) => {
    return await axios.post("http://127.0.0.1:3000/geolocation/upload", fileToFormData(fileCsv), getHeaders());
}

export let postClassifierUpload = async (fileCsv: File) => {
    return await axios.post("http://127.0.0.1:3000/classifier/upload", fileToFormData(fileCsv), getHeaders());
}